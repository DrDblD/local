class Term < ActiveRecord::Base
validates :name, uniqueness: { case_sensitive: false }
has_many :relateds
accepts_nested_attributes_for :relateds
end
