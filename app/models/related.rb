class Related < ActiveRecord::Base
	belongs_to :term
	belongs_to :term, class_name: "Term",
                        foreign_key: "related_id"
end
